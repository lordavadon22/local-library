#Проект локальной библиотеке

Для запуска проекта на локальной машине:

1. Установить виртуальное окружение ```python3 -m venv venv```
2. Перейти в виртуальное окружение ```source /venv/bin/activate``` (для Linux) или ```venv\Scripts\activate``` (для Windows)
3. Установить Django ```pip install django```
4. Выполнить миграции ```python manage.py migrate```
5. Создать суперпользователя ```python manage.py createsuperuser```
6. Запустить сервер ```python manage.py runserver``

