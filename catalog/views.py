from django.shortcuts import render
from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.shortcuts import get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth.decorators import permission_required
import datetime
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy

from .forms import RenewBookForm
from .models import Book, BookInstance, Author, Genre


def index(request):
    """Функция отображения домашней страницы сайта."""
    num_books = Book.objects.all().count()
    num_instances = BookInstance.objects.all().count()
    num_instances_available = BookInstance.objects.filter(status__exact='a').count()
    num_authors = Author.objects.count()

    num_visits = request.session.get('num_visits', 0)
    request.session['num_visits'] = num_visits + 1

    context = {
        'num_books': num_books,
        'num_instances': num_instances,
        'num_instances_available': num_instances_available,
        'num_authors': num_authors,
        'num_visits': num_visits
    }

    return render(
        request,
        'index.html',
        context=context
    )


@permission_required('catalog.can_mark_returned')
def renew_book_librarian(request, pk):
    """Функция для обновления определенного экземпляра BookInstance библиотекарем"""
    book_inst = get_object_or_404(BookInstance, pk=pk)

    if request.method == 'POST':
        form = RenewBookForm(request.POST)
        if form.is_valid():
            book_inst.due_back = form.cleaned_data['renewal_date']
            book_inst.save()
            return HttpResponseRedirect(reverse('borrowed-list'))
    else:
        proposed_renewal_date = datetime.date.today() + datetime.timedelta(weeks=3)
        form = RenewBookForm(initial={'renewal_date': proposed_renewal_date})

    return render(request, 'catalog/book_renew_librarian.html', {'form': form, 'bookinst': book_inst})


class BookListView(generic.ListView):
    """отображение списка книг"""
    model = Book
    paginate_by = 10


class BorrowedListView(LoginRequiredMixin, PermissionRequiredMixin, generic.ListView):
    paginate_by = 10
    permission_required = 'catalog.can_mark_returned'
    template_name = 'catalog/borrowed_list.html'
    queryset = BookInstance.objects.filter(status__exact='o').order_by('due_back')


class BookDetailView(generic.DetailView):
    """отображение детальной информации о книги"""
    model = Book


class AuthorListView(generic.ListView):
    """отображение списка авторов"""
    model = Author
    paginate_by = 10


class AuthorDetailView(generic.DetailView):
    """отображение детальной информации об авторе книги"""
    model = Author


class LoanedBooksByUserListView(LoginRequiredMixin, generic.ListView):
    """Общий просмотр на основе классов со списком книг, предоставленных текущему пользователю."""
    model = BookInstance
    template_name ='catalog/bookinstance_list_borrowed_user.html'
    paginate_by = 10

    def get_queryset(self):
        return BookInstance.objects.filter(borrower=self.request.user).filter(status__exact='o').order_by('due_back')


class AuthorCreate(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    model = Author
    fields = '__all__'
    initial = {'date_of_death': datetime.datetime.today()}
    permission_required = 'catalog.can_mark_returned'


class AuthorUpdate(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    model = Author
    fields = ['first_name', 'last_name', 'date_of_birth', 'date_of_death']
    permission_required = 'catalog.can_mark_returned'


class AuthorDelete(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    model = Author
    success_url = reverse_lazy('authors')
    permission_required = 'catalog.can_mark_returned'


class BookCreate(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    model = Book
    fields = '__all__'
    permission_required = 'catalog.can_mark_returned'


class BookUpdate(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    model = Book
    fields = '__all__'
    permission_required = 'catalog.can_mark_returned'


class BookDelete(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    model = Book
    success_url = reverse_lazy('books')
    permission_required = 'catalog.can_mark_returned'
