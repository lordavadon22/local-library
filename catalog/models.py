import uuid

from django.db import models
from django.urls import reverse
from django.contrib.auth.models import User

from datetime import date


class Genre(models.Model):
    """Модель, представляющая жанр книги (например, научная фантастика, научно-популярная литература)."""
    name = models.CharField('Название жанра', max_length=200,
                            help_text='Введите жанр книги(Фантастика, Фентези, Детектив и т.д.)')

    class Meta:
        verbose_name = "Жанр"
        verbose_name_plural = "Жанры"

    def __str__(self):
        """Строковое отображение модели (в админ панели и т.д.)"""
        return self.name


class Language(models.Model):
    """Модель, представляющая язык (например, английский, французский, японский и т. Д.)"""
    name = models.CharField('Название языка', max_length=200,
                            help_text="Введите естественный язык книги "
                                      "(например, английский, французский, японский и т.д.)")

    class Meta:
        verbose_name = 'Язык'
        verbose_name_plural = 'Языки'

    def __str__(self):
        """Строковое отображение модели (в админ панели и т.д.)"""
        return self.name


class Book(models.Model):
    """Модель, представляющая книгу (но не конкретный экземпляр книги)."""
    title = models.CharField('Название книги', max_length=200)
    author = models.ForeignKey('Author', on_delete=models.SET_NULL, null=True, verbose_name='Автор')
    summary = models.TextField('Краткое описание', max_length=1000, help_text='Введите краткое описание книги')
    isbn = models.CharField('isbn', max_length=13,
                            help_text='13-символьный '
                                      '<a href="https://www.isbn-international.org/content/what-isbn"> '
                                      'номер ISBN </a>')
    genre = models.ManyToManyField(Genre, help_text='Выберите жанр для этой книги', verbose_name='Жанры')
    language = models.ForeignKey(Language, on_delete=models.SET_NULL, null=True, verbose_name='Язык')

    class Meta:
        verbose_name = "Книга(-у)"
        verbose_name_plural = "Книги"

    def __str__(self):
        """Строковое отображение модели (в админ панели и т.д.)"""
        return self.title

    def get_absolute_url(self):
        """Возвращает URL-адрес для доступа к конкретному экземпляру книги."""
        return reverse('book-detail', args=[str(self.id)])


class BookInstance(models.Model):
    """Модель, представляющая конкретный экземпляр книги (то есть который можно взять из библиотеки)."""
    id = models.UUIDField(primary_key=True, default=uuid.uuid4,
                          help_text="Уникальный идентификатор этой конкретной книги во всей библиотеке")
    book = models.ForeignKey('Book', on_delete=models.SET_NULL, null=True, verbose_name='Книга')
    imprint = models.CharField('Печатное издание', max_length=200)
    due_back = models.DateField('Дата возврата', null=True, blank=True)

    LOAN_STATUS = (
        ('m', 'Не доступна'),
        ('o', 'Выдана'),
        ('a', 'Доступна'),
        ('r', 'Зарезервирована'),
    )

    status = models.CharField('Статус', max_length=1, choices=LOAN_STATUS, blank=True,
                              default='m', help_text='Доступность книги')
    borrower = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True, verbose_name='Читатель')

    class Meta:
        verbose_name = "Экземпляр книги"
        verbose_name_plural = "Экземпляры книг"
        permissions = (("can_mark_returned", "Set book as returned"),)

    def __str__(self):
        """Строковое отображение модели (в админ панели и т.д.)"""
        return f'{self.id} ({self.book.title})'

    @property
    def is_overdue(self):
        return self.due_back and date.today() > self.due_back


class Author(models.Model):
    """Модель, представляющая автора."""
    first_name = models.CharField('Имя', max_length=100)
    last_name = models.CharField('Фамилия', max_length=100)
    date_of_birth = models.DateField('Дата рождения', null=True, blank=True)
    date_of_death = models.DateField('Дата смерти', null=True, blank=True)

    class Meta:
        verbose_name = "Автор"
        verbose_name_plural = "Авторы"

    def get_absolute_url(self):
        """Возвращает URL-адрес для доступа к конкретному экземпляру автора."""
        return reverse('author-detail', args=[str(self.id)])

    def __str__(self):
        """Строковое отображение модели (в админ панели и т.д.)"""
        return f'{self.last_name}, { self.first_name}'
