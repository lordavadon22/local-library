from django.test import TestCase
from django.db.models import prefetch_related_objects

from ._prepare_test import (create_author,
                            get_author,
                            get_genre,
                            create_genre,
                            create_language,
                            get_language,
                            create_book,
                            get_book, create_book_instance, get_book_instance)


class AuthorModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        create_author()

    def test_first_name_label(self):
        author = get_author()
        field_label = author._meta.get_field('first_name').verbose_name
        self.assertEquals(field_label, 'Имя')

    def test_last_name_label(self):
        author = get_author()
        field_label = author._meta.get_field('last_name').verbose_name
        self.assertEquals(field_label, 'Фамилия')

    def test_date_of_birth_label(self):
        author = get_author()
        field_label = author._meta.get_field('date_of_birth').verbose_name
        self.assertEquals(field_label, 'Дата рождения')

    def test_date_of_death_label(self):
        author = get_author()
        field_label = author._meta.get_field('date_of_death').verbose_name
        self.assertEquals(field_label, 'Дата смерти')

    def test_first_name_max_length(self):
        author = get_author()
        max_length = author._meta.get_field('first_name').max_length
        self.assertEquals(max_length, 100)

    def test_last_name_max_length(self):
        author = get_author()
        max_length = author._meta.get_field('last_name').max_length
        self.assertEquals(max_length, 100)

    def test_object_name_is_last_name_comma_first_name(self):
        author = get_author()
        expected_object_name = f'{author.last_name}, { author.first_name}'
        self.assertEquals(expected_object_name, str(author))

    def test_get_absolute_url(self):
        author = get_author()
        self.assertEquals(author.get_absolute_url(), '/catalog/author/1/')


class GenreModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        create_genre()

    def test_name_label(self):
        genre = get_genre()
        field_label = genre._meta.get_field('name').verbose_name
        self.assertEquals(field_label, 'Название жанра')

    def test_name_max_length(self):
        genre = get_genre()
        max_length = genre._meta.get_field('name').max_length
        self.assertEquals(max_length, 200)

    def test_object_name_is_name(self):
        genre = get_genre()
        expected_object_name = genre.name
        self.assertEquals(expected_object_name, str(genre))


class LanguageModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        create_language()

    def test_name_label(self):
        language = get_language()
        field_label = language._meta.get_field('name').verbose_name
        self.assertEquals(field_label, 'Название языка')

    def test_name_max_length(self):
        language = get_language()
        max_length = language._meta.get_field('name').max_length
        self.assertEquals(max_length, 200)

    def test_object_name_is_name(self):
        language = get_language()
        expected_object_name = language.name
        self.assertEquals(expected_object_name, str(language))


class BookModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        create_book()

    def test_title_label(self):
        book = get_book()
        field_label = book._meta.get_field('title').verbose_name
        self.assertEquals(field_label, 'Название книги')

    def test_summary_label(self):
        book = get_book()
        field_label = book._meta.get_field('summary').verbose_name
        self.assertEquals(field_label, 'Краткое описание')

    def test_isbn_label(self):
        book = get_book()
        field_label = book._meta.get_field('isbn').verbose_name
        self.assertEquals(field_label, 'isbn')

    def test_author_label(self):
        book = get_book()
        field_label = book._meta.get_field('author').verbose_name
        self.assertEquals(field_label, 'Автор')

    def test_genre_label(self):
        book = get_book()
        field_label = book._meta.get_field('genre').verbose_name
        self.assertEquals(field_label, 'Жанры')

    def test_language_label(self):
        book = get_book()
        field_label = book._meta.get_field('language').verbose_name
        self.assertEquals(field_label, 'Язык')

    def test_title_max_length(self):
        book = get_book()
        max_length = book._meta.get_field('title').max_length
        self.assertEquals(max_length, 200)

    def test_summary_max_length(self):
        book = get_book()
        max_length = book._meta.get_field('summary').max_length
        self.assertEquals(max_length, 1000)

    def test_isbn_max_length(self):
        book = get_book()
        max_length = book._meta.get_field('isbn').max_length
        self.assertEquals(max_length, 13)

    def test_object_name_is_title(self):
        book = get_book()
        expected_object_name = book.title
        self.assertEquals(expected_object_name, str(book))

    def test_get_absolute_url(self):
        book = get_book()
        self.assertEquals(book.get_absolute_url(), '/catalog/book/1/')


class BookInstanceModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        create_book_instance()

    def test_book_label(self):
        book_instance = get_book_instance()
        field_label = book_instance._meta.get_field('book').verbose_name
        self.assertEquals(field_label, 'Книга')

    def test_imprint_label(self):
        book_instance = get_book_instance()
        field_label = book_instance._meta.get_field('imprint').verbose_name
        self.assertEquals(field_label, 'Печатное издание')

    def test_due_back_label(self):
        book_instance = get_book_instance()
        field_label = book_instance._meta.get_field('due_back').verbose_name
        self.assertEquals(field_label, 'Дата возврата')

    def test_status_label(self):
        book_instance = get_book_instance()
        field_label = book_instance._meta.get_field('status').verbose_name
        self.assertEquals(field_label, 'Статус')

    def test_borrower_label(self):
        book_instance = get_book_instance()
        field_label = book_instance._meta.get_field('borrower').verbose_name
        self.assertEquals(field_label, 'Читатель')

    def test_imprint_max_length(self):
        book_instance = get_book_instance()
        max_length = book_instance._meta.get_field('imprint').max_length
        self.assertEquals(max_length, 200)

    def test_status_max_length(self):
        book_instance = get_book_instance()
        max_length = book_instance._meta.get_field('status').max_length
        self.assertEquals(max_length, 1)

    def test_object_name_is_id_comma_title(self):
        book_instance = get_book_instance()
        expected_object_name = f'{book_instance.id} ({book_instance.book.title})'
        self.assertEquals(expected_object_name, str(book_instance))

    def test_book_instance_is_overdue(self):
        book_instance = get_book_instance()
        self.assertTrue(book_instance.is_overdue)
