import datetime
from django.contrib.auth.models import User

from catalog.models import Author, Genre, Language, Book, BookInstance


def create_author():
    return Author.objects.create(first_name='Big', last_name='Bob')


def get_author():
    return Author.objects.get(id=1)


def create_genre():
    return Genre.objects.create(name='Фантастика')


def get_genre():
    return Genre.objects.get(id=1)


def create_language():
    return Language.objects.create(name='Русский')


def get_language():
    return Language.objects.get(id=1)


def create_book():
    author = create_author()
    genre = create_genre()
    language = create_language()
    book = Book.objects.create(author=author,
                               language=language,
                               title='Морозко',
                               summary='Сказка про Морозко',
                               isbn='1234567891011')
    book.genre.add(genre)
    return book


def get_book():
    return Book.objects.get(id=1)


def create_book_instance():
    book = create_book()
    borrower = User.objects.create(username='Zip', password='Zap')
    return BookInstance.objects.create(book=book, imprint='Мир книг',
                                       due_back=datetime.date(2021, 10, 10),
                                       borrower=borrower)


def get_book_instance():
    return BookInstance.objects.first()

